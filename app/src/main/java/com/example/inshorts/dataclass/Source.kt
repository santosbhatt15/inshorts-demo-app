package com.example.inshorts.dataclass

data class Source(
    val id: String,
    val name: String
)