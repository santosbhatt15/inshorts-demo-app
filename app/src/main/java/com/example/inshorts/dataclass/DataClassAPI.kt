package com.example.inshorts.dataclass

data class DataClassAPI(
    val articles: List<Article>,
    val status: String,
    val totalResults: Int
)