package com.example.inshorts.networkcalls

import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitHelper {

    const val BASE_URL="https://newsapi.org/"

    fun getInstance(): Retrofit {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            // we need to add converter factory to
            // convert JSON object to Java object
            .build()
    }
    //https://newsapi.org/
    //https://gnews.io/api/
}