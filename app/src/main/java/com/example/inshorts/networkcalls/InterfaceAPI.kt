package com.example.inshorts.networkcalls

import com.example.inshorts.dataclass.DataClassAPI
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface InterfaceAPI {

    @GET("v2/top-headlines")     //currently calling this api in code
    suspend fun getBreakingNews(
        @Query("country")
        countryCode: String = "us",
        @Query("page")
        pageNumber: Int = 1,
        @Query("apiKey")
        apiKey: String = "13c46d35a1924360a816fc673cdcee8a"
    ): Response<DataClassAPI>

    //https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=API_KEY
    //5b2984439cd745a6aec82a9e19569209
    //https://gnews.io/api/v4/search?q=example&token=API-Token
    //13c46d35a1924360a816fc673cdcee8a
}