package com.example.inshorts.ui

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.viewpager2.widget.ViewPager2
import com.example.inshorts.ui.adapters.CardViewTransformer
import com.example.inshorts.R
import com.example.inshorts.ui.adapters.ViewPager2Adapter
import com.example.inshorts.dataclass.Article
import com.example.inshorts.dataclass.DataClassAPI
import com.example.inshorts.networkcalls.InterfaceAPI
import com.example.inshorts.networkcalls.RetrofitHelper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeFragment : Fragment() {

    var articles= mutableListOf<Article>()
    var totalResults=0
    var totalCounts=0
    var page=1
    var currentListSize=15
    var activityCont: Context? =null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activityCont=context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.setOnClickListener {
            Toast.makeText(context, "Hello", Toast.LENGTH_SHORT).show()
        }
            setViewPager2() //setting viewpager
 //           getData() //hitting api

    }

    private fun setViewPager2() {

        viewPager2.apply {
            adapter = ViewPager2Adapter(activityCont,articles)
            orientation = ViewPager2.ORIENTATION_VERTICAL
            setPageTransformer(CardViewTransformer())

        }
    }

    fun getData() {

        val newsApi = RetrofitHelper.getInstance().create(InterfaceAPI::class.java)

        CoroutineScope(Dispatchers.IO).launch{
           val result = newsApi.getBreakingNews("in", page,"13c46d35a1924360a816fc673cdcee8a")
//            val result=newsApi.getData(page,"13c46d35a1924360a816fc673cdcee8a")

        Log.e("NumPage", page.toString())

            if (result != null){
               // Checking the results
               val myData: DataClassAPI? = result.body()
               Log.e("TAG","Inside getData() ${myData?.totalResults}")


               toMainThread(myData) //calling this method to run viewpageradapter on main thread

           }
       }

    }

    private fun toMainThread(myData: DataClassAPI?) {

        Handler(Looper.getMainLooper()).post {

            myData?.articles?.let { articles.addAll(it) }

            Log.e("Articles",articles[0].title)
            viewPager2.adapter?.notifyDataSetChanged()  //to notify viewpager for the new data
            totalResults= myData?.totalResults!!
            Log.e("TAG","Handler Running")


            viewPager2.apply {
                registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

                    override fun onPageScrolled(
                        position: Int,
                        positionOffset: Float,
                        positionOffsetPixels: Int
                    ) {
                        super.onPageScrolled(position, positionOffset, positionOffsetPixels)
//                        if (position>= 7 && totalCounts<totalResults){
//                            page++
//                            totalCounts++
//                            Log.e("Counting position", position.toString())
//
//                            getData()
//                        }
                    }


                    override fun onPageSelected(position: Int) {
                        super.onPageSelected(position)

                        view?.setOnClickListener {
                            activity?.appBar?.visibility = View.INVISIBLE
                        }

                        Log.e("onPageSelected","position "+position.toString())
                        if (position>= currentListSize && totalCounts<totalResults){
                            currentListSize+=15
                            Log.e("CurrentListsize",currentListSize.toString())
                            page++
                            totalCounts++
                            Log.e("Counting position", position.toString())

                            getData()
                        }
                    }
                })
            }
        }
    }
}