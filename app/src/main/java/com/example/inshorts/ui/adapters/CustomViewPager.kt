package com.example.inshorts.ui.adapters

import android.content.Context
import android.util.AttributeSet
import androidx.viewpager.widget.ViewPager
import android.view.MotionEvent

class CustomViewPager(context: Context?, attrs: AttributeSet?) : ViewPager(
    context!!, attrs
) {
    private var enabledT:Boolean = true

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (enabledT) {
            super.onTouchEvent(event)
        } else false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (enabledT) {
            super.onInterceptTouchEvent(event)
        } else false
    }

    fun setPagingEnabled(enabled: Boolean) {
        this.enabledT = enabled
    }
}