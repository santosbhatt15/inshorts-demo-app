package com.example.inshorts.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.inshorts.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_discover_items.view.*


class DiscoverItems : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_discover_items, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.iv_quote.setOnClickListener {
            activity?.appBar?.visibility = View.GONE
            activity?.viewPager?.setPagingEnabled(false)    //disabling viewpager swipe
            activity?.supportFragmentManager?.beginTransaction()
                ?.add(R.id.discoverItemsFragment, QuotesViewPagerFragment())?.addToBackStack(null)?.
                commit()

        }
    }

}