package com.example.inshorts.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.example.inshorts.R
import com.example.inshorts.ui.adapters.CardViewTransformer
import com.example.inshorts.ui.adapters.QuotesViewPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_quotes_view_pager.*


class QuotesViewPagerFragment : Fragment() {

    var imageList = arrayListOf<Int>()
    var activityContext: Context? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activityContext=context
        Log.e("Fragment Context", context.toString())
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_quotes_view_pager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addImage()   //adding images in the arrayList
        initViewPager2() //intialiazing viewpager

    }


    private fun addImage() {
        imageList.add(R.drawable.quotes1)
        imageList.add(R.drawable.quotes2)
        imageList.add(R.drawable.quotes3)
        imageList.add(R.drawable.quotes4)
        imageList.add(R.drawable.quotes5)
        imageList.add(R.drawable.quotes6)
        imageList.add(R.drawable.quotes7)
        imageList.add(R.drawable.quotes8)
        imageList.add(R.drawable.quotes9)
        imageList.add(R.drawable.quotes10)
    }

    private fun initViewPager2() {

        vp2Quotes.apply {
            adapter = QuotesViewPagerAdapter(activityContext,imageList)
            Log.e("TAG","viewPager2 ")
            orientation = ViewPager2.ORIENTATION_VERTICAL
            setPageTransformer(CardViewTransformer())

        }
    }

    override fun onStop() {
        super.onStop()
        //setting visibility visible and viewpager enable on destroy of this fragment
        activity?.appBar?.visibility = View.VISIBLE
        activity?.viewPager?.setPagingEnabled(true)
    }

}