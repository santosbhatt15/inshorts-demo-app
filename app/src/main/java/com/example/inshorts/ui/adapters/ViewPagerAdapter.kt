package com.example.inshorts.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.RecyclerView



class ViewPagerAdapter(supportFragmentManager: FragmentManager)
    :FragmentPagerAdapter(supportFragmentManager) {

    private val list=ArrayList<Fragment>()  //list to contain fragments

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Fragment {
        return list[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when(position){
            0->return "Discover"
            1->return "My Feed"
        }
        return super.getPageTitle(position)
    }

    // method to add fragments in list
    fun addFragment(fragment: Fragment)
    {
        list.add(fragment)
    }

}
