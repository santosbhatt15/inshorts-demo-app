package com.example.inshorts.ui.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.news.view.*
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.example.inshorts.R
import com.example.inshorts.dataclass.Article


class ViewPager2Adapter(var activityCont: Context?, private val list: MutableList<Article>) :
    PagingDataAdapter<Article, ViewPager2Adapter.PagerVH>(
    DiffUtilCallBack()
) {


    class PagerVH(itemView: View) : RecyclerView.ViewHolder(itemView)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerVH {

        val view= LayoutInflater.from(parent.context).inflate(R.layout.news, parent, false)
        Log.e("TAG","oncreateViewHolder")
        return (PagerVH(view))

    }

    override fun getItemCount(): Int{
        Log.e("List size: ",list.size.toString())
        return list.size
    }

    override fun onBindViewHolder(holder: PagerVH, position: Int) = holder.itemView.run {
        Log.e("TAG","onbindview")


        tv_heading.text=list[position].title

        Glide.with(context)
            .load(list[position].urlToImage)
            .into(iv_news)

        tv_subHeading.text = list[position].description

        tv_toUrl.setOnClickListener {
            Intent(Intent.ACTION_VIEW).also{
                it.setData(Uri.parse(list[position].url))   //taking user to the news url
                context.startActivity(it)
            }
        }

//        container.setBackgroundResource(colors[position])
    }
    class DiffUtilCallBack: DiffUtil.ItemCallback<Article>() {
        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.title == newItem.title
                    && oldItem.title == newItem.title
        }

    }

}
