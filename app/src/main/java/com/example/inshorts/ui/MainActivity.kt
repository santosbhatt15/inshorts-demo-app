package com.example.inshorts.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.inshorts.R
import com.example.inshorts.ui.adapters.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        Log.e("Activity Context",this.toString())

        val fragmentHome= HomeFragment()      //making object of HomeFragment
        val fragmentDiscover= DiscoverItems()  // making object of DiscoverItems

        val viewPageAdapter = ViewPagerAdapter(supportFragmentManager)
        viewPageAdapter.addFragment(fragmentDiscover) //adding DiscoverItems to viewpagerAdapter
        viewPageAdapter.addFragment(fragmentHome)    //adding HomeFragment to viewpagerAdapter


        viewPager.adapter=viewPageAdapter   //attaching viewpagerAdapter to viewpager
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.getTabAt(1)?.select()

    }

    fun clickListener(){
        Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show()
    }
}