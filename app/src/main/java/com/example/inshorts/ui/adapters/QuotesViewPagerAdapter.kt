package com.example.inshorts.ui.adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.inshorts.R
import com.example.inshorts.dataclass.Article
import kotlinx.android.synthetic.main.quotes.view.*

class QuotesViewPagerAdapter(var activityCont: Context?, var imageList: ArrayList<Int>)
    : PagingDataAdapter<Article, QuotesViewPagerAdapter.PagerVH>(
DiffUtilCallBack()
) {


   inner class PagerVH(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerVH {

        val view= LayoutInflater.from(parent.context).inflate(R.layout.quotes, parent, false)
        Log.e("TAAG","oncreateViewHolder")
        return (PagerVH(view))

    }

    override fun getItemCount(): Int{
        Log.e("List size: ",imageList.size.toString())
        return imageList.size
    }

    override fun onBindViewHolder(holder: PagerVH, position: Int){
        Log.e("TAG","onbindview")

//        iv_quotes.setAltImageResource(imageList[position])

        Glide.with(holder.itemView)
            .load(imageList[position])
            .into(holder.itemView.iv_quotes)

        holder.itemView.setOnClickListener {

            Intent(Intent.ACTION_SEND).also {
                it.putExtra(Intent.EXTRA_TEXT,"MY message")
                it.type="text/plain"
                activityCont?.startActivity(it)
            }

        }
    }



    class DiffUtilCallBack: DiffUtil.ItemCallback<Article>() {
        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.title == newItem.title
                    && oldItem.title == newItem.title
        }

    }

}
